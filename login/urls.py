from django.conf.urls import url
from .views import LoginPage, signup, signang

urlpatterns = [
    url('login', LoginPage),
    url('signup', signup),
    url('anggota', signang)
]