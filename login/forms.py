from django import forms
import math
class Login_Form(forms.Form):
    no_ktp = forms.CharField(label='Nomor KTP', max_length=20, required=True, widget=forms.TextInput(attrs={
        'id':'form',
        'placeholder':'Nomor KTP Anda'
    }))

    email = forms.EmailField(label='Email', max_length=200, required=True, widget=forms.TextInput(attrs={
        'id':'form',
        'placeholder':'email@anda.com'
    }))

class Signup_Form_Admin(forms.Form):
    NoKTP_attrs={
        'class':'form-control',
        'placeholder': 'Nomor KTP Anda'
    }
    fullname_attrs={
        'class':'form-control',
        'placeholder': 'Nama lengkap Anda'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    dob_attrs={
        'class':'form-control',
        'placeholder': 'MM-DD-YYYY'
    }
    telp_attrs={
        'class':'form-control',
        'placeholder': 'Nomor telepon Anda'
    }
    fullname=forms.CharField(
        label='Fullname',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=fullname_attrs))

    NoKTP=forms.CharField(
        label='NoKTP',
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs=NoKTP_attrs))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    dateofbirth=forms.CharField(
        label='Tanggal Lahir',
        required=True,
        widget=forms.TextInput(attrs=dob_attrs))
    
    telpon=forms.CharField(
        label='Nomor Telepon',
        required=True,
        widget=forms.TextInput(attrs=telp_attrs))

class Signup_Form_Anggota(forms.Form):
    NoKTP_attrs={
        'class':'form-control',
        'placeholder': 'Nomor KTP Anda'
    }
    fullname_attrs={
        'class':'form-control',
        'placeholder': 'Nama lengkap Anda'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    dob_attrs={
        'class':'form-control',
        'placeholder': 'MM-DD-YYYY'
    }
    telp_attrs={
        'class':'form-control',
        'placeholder': 'Nomor telepon Anda'
    }
    nalamat_attrs={
        'id':'form',
        'placeholder': 'Nama'
    }
    jalamat_attrs={
        'id':'form',
        'placeholder': 'Nama Jalan'
    }
    noalamat_attrs={
        'id':'form',
        'placeholder': str(math.pi)
    }
    kotalamat_attrs={
        'id':'form',
        'placeholder':'Nama kota Anda'
    }
    kodepos_attrs={
        'id':'form',
        'placeholder':'Kodepos'
    }
    fullname=forms.CharField(
        label='Fullname',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=fullname_attrs))

    NoKTP=forms.CharField(
        label='NoKTP',
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs=NoKTP_attrs))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    dateofbirth=forms.CharField(
        label='Tanggal Lahir',
        required=True,
        widget=forms.TextInput(attrs=dob_attrs))
    
    telpon=forms.CharField(
        label='Nomor Telepon',
        required=True,
        widget=forms.TextInput(attrs=telp_attrs))
    
    alamat=forms.CharField(
        label='Nama Alamat',
        required=True,
        widget=forms.TextInput(attrs=nalamat_attrs))
    
    jalan=forms.CharField(
        label='Nama Jalan',
        required=True,
        widget=forms.TextInput(attrs=jalamat_attrs))
    
    nalamat=forms.CharField(
        label='Nomor',
        required=True,
        widget=forms.TextInput(attrs=nalamat_attrs))
    
    kota=forms.CharField(
        label='Kota',
        required=True,
        widget=forms.TextInput(attrs=kotalamat_attrs))

    kodepos=forms.CharField(
        label='Kodepos',
        required=True,
        widget=forms.TextInput(attrs=kodepos_attrs))
    