from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import *
import urllib.request

def LoginPage(request):
	return render(request, 'Login.html')

def signup(request):
	return render(request, 'signup.html')

def signang(request):
	return render(request, 'signup-anggota.html')

def logout(request):
	request.session.flush()
	return HttpResponseRedirect('/')
