from django import forms

class FormBarang(forms.Form):
    id_barang_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi ID Barang'
    }
    nama_item_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi nama item'
    }
    warna_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi warna barang'
    }
    url_foto_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi url foto'
    }
    kondisi_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi kondisi barang'
    }
    lama_penggunaan_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi lama penggunaan'
    }
    pemilik_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi nama pemilik/penyewa'
    }
    id_barang = forms.CharField(label="ID Barang", required=True, max_length=100, widget=forms.TextInput(attrs=id_barang_attrs))
    nama_item = forms.EmailField(label="Nama Item", required=True, max_length=100, widget=forms.TextInput(attrs=nama_item_attrs))
    warna = forms.CharField(label="Warna", required=True, max_length=100, widget=forms.TextInput(attrs=warna_attrs))
    url_foto = forms.CharField(label="URL Foto", required=True, max_length=100, widget=forms.TextInput(attrs=url_foto_attrs))
    kondisi = forms.CharField(label="Kondisi", required=True, max_length=100, widget=forms.TextInput(attrs=kondisi_attrs))
    lama_penggunaan = forms.CharField(label="Lama Penggunaan", required=True, max_length=100, widget=forms.TextInput(attrs=lama_penggunaan_attrs))
    pemilik = forms.CharField(label="Pemilik", required=True, max_length=100, widget=forms.TextInput(attrs=pemilik_attrs))


class UpdateBarangForm(forms.Form):
    id_barang_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi ID Barang'
    }
    nama_item_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi nama item'
    }
    warna_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi warna barang'
    }
    url_foto_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi url foto'
    }
    kondisi_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi kondisi barang'
    }
    lama_penggunaan_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi lama penggunaan'
    }
    pemilik_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi nama pemilik/penyewa'
    }
    id_barang = forms.CharField(label="ID Barang", required=True, max_length=100, widget=forms.TextInput(attrs=id_barang_attrs))
    nama_item = forms.EmailField(label="Nama Item", required=True, max_length=100, widget=forms.TextInput(attrs=nama_item_attrs))
    warna = forms.CharField(label="Warna", required=True, max_length=100, widget=forms.TextInput(attrs=warna_attrs))
    url_foto = forms.CharField(label="URL Foto", required=True, max_length=100, widget=forms.TextInput(attrs=url_foto_attrs))
    kondisi = forms.CharField(label="Kondisi", required=True, max_length=100, widget=forms.TextInput(attrs=kondisi_attrs))
    lama_penggunaan = forms.CharField(label="Lama Penggunaan", required=True, max_length=100, widget=forms.TextInput(attrs=lama_penggunaan_attrs))
    pemilik = forms.CharField(label="Pemilik", required=True, max_length=100, widget=forms.TextInput(attrs=pemilik_attrs))