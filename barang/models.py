from django.db import models

class createBarang(models.Model):
    id_barang = models.CharField(max_length=100)
    nama_item = models.CharField(max_length=100)
    warna = models.CharField(max_length=100)
    url_foto = models.CharField(max_length=100)
    kondisi = models.CharField(max_length=100)
    lama_penggunaan = models.CharField(max_length=100)
    pemilik = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class UpdateBarang(models.Model):
    id_barang = models.CharField(max_length=100)
    nama_item = models.CharField(max_length=100)
    warna = models.CharField(max_length=100)
    url_foto = models.CharField(max_length=100)
    kondisi = models.CharField(max_length=100)
    lama_penggunaan = models.CharField(max_length=100)
    pemilik = models.CharField(max_length=100)

    def __str__(self):
        return self.title