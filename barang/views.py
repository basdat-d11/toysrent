from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.db import connection
from .forms import FormBarang, UpdateBarangForm
from .models import createBarang, UpdateBarang

response = {}

def create_barang(request):
    html = 'create_barang.html'
    response['fill_create_barang'] = FormBarang
    return render(request, html, response)
	
def fill_create_barang(request):
    if (request.method == 'POST'):
        response['id_barang'] = request.POST['id_barang'] 
        response['nama_item'] = request.POST['nama_item'] 
        response['warna'] = request.POST['warna']
        response['url_foto'] = request.POST['url_foto']
        response['kondisi'] = request.POST['kondisi']
        response['lama_penggunaan'] = request.POST['lama_penggunaan']
        response['pemilik'] = request.POST['pemilik']
        pesan = createBarang(id_barang=response['id_barang'], nama_item=response['nama_item'], 
							warna=response['warna'], url_foto=response['url_foto'], kondisi=response['kondisi'],
                            lama_penggunaan=response['lama_penggunaan'], pemilik=response['pemilik'] )
        pesan.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/create_barang/')

def delete_barang(request):
	return render(request, 'delete_barang.html', response)

def update_barang(request):
    html = 'update_barang.html'
    response['fill_update_barang'] = UpdateBarangForm
    return render(request, html, response)
	
def fill_update_barang(request):
    if (request.method == 'POST'): 
        response['id_barang'] = request.POST['id_barang'] 
        response['nama_item'] = request.POST['nama_item'] 
        response['warna'] = request.POST['warna']
        response['url_foto'] = request.POST['url_foto']
        response['kondisi'] = request.POST['kondisi']
        response['lama_penggunaan'] = request.POST['lama_penggunaan']
        response['pemilik'] = request.POST['pemilik']
        update = createBarang(id_barang=response['id_barang'], nama_item=response['nama_item'], 
							warna=response['warna'], url_foto=response['url_foto'], kondisi=response['kondisi'],
                            lama_penggunaan=response['lama_penggunaan'], pemilik=response['pemilik'] )
        update.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/update_barang/')

def detail_barang(request):
    return render(request, "detail_barang.html")

def daftar_barang(request):
	response = {}
	cursor = connection.cursor()
	cursor.execute('set search_path to toysrent_d11')
	cursor.execute("select * from barang;")
	
	hasil1 = cursor.fetchall()
	response['barang'] = hasil1
	return render(request, 'daftar_barang.html', response)
