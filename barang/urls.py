from django.conf.urls import url
from .views import daftar_barang, create_barang, delete_barang, update_barang, detail_barang

urlpatterns = [
    url('daftar_barang', daftar_barang),
    url('delete_barang', delete_barang),
    url('create_barang', create_barang),
    url('update_barang', update_barang),
    url('detail_barang', detail_barang)
]