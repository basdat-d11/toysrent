from django.db import connection
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import LevelForm

response = {}

def level(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    if request.method == 'POST':
        form = LevelForm(request.POST)
        if form.is_valid():
            nama = form.cleaned_data['nama']
            poin = form.cleaned_data['poin']
            deskripsi = form.cleaned_data['deskripsi']
            cursor.execute("insert into level_keanggotaan values ('" + nama + "', " + str(poin) + ", '" + deskripsi + "')")
            return HttpResponseRedirect('/level/' + level)
    else:
        form = LevelForm()
    return render(request,'tambah_level.html',{'forms' : form})

def update(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    if request.method == 'POST':
        form = LevelForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('')
    else:
        form = LevelForm()
    return render(request,'update_level.html',{'forms' : form})


def konfirmasi(request):
    return render(request, 'konfirmasi.html', response)


def daftar_level(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    cursor.execute('select * from level_keanggotaan')
    result = cursor.fetchall()
    response['levels'] = result
    return render(request,'daftar_level.html',response)

