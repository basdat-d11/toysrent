from django.urls import path
from .views import level, update, konfirmasi, daftar_level

urlpatterns = [
    path('tambah_level', level, name="level"),
    path('update_level', update, name="update"),
    path('konfirmasi', konfirmasi, name="konfirmasi"),
    path('daftar_level', daftar_level, name="daftar_level")
]