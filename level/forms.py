from django import forms

class LevelForm(forms.Form):
    nama_attrs = {
        'id': 'nama',
        'class' : 'form-control',
        'placeholder': 'Isi nama level yang ingin ditambahkan'
    }
    poin_attrs = {
        'id': 'poin',
        'class' : 'form-control',
        'placeholder': 'Isi minimum poin yang dibutuhkan'
    }
    desk_attrs = {
        'id': 'deskripsi',
        'class' : 'form-control',
        'placeholder': 'Deskripsi level'
    }
    nama = forms.CharField(label="Nama Level", required=True, max_length=100, widget=forms.TextInput(attrs=nama_attrs))
    poin = forms.IntegerField(label="Minimum Poin", required=True, min_value=0, widget=forms.TextInput(attrs=poin_attrs))
    deskripsi = forms.CharField(label="Deskripsi", required=True, min_length=100, widget=forms.TextInput(attrs=desk_attrs))
