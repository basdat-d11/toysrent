from django.conf.urls import url
from .views import chat, chatanggota

urlpatterns = [
	url('chatanggota', chatanggota),
	url('', chat)
]