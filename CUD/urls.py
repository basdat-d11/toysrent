from django.urls import path
from .views import item, fill_form, update, fill_form2, daftar_item, daftar_item, konfirmasi_item, delete_item

urlpatterns = [
    path('item', item, name="item"),
    path('update_item', update, name="update"),
    path('daftar_item', daftar_item, name="daftar_item"),
    path('delete_item', delete_item, name="delete_item"),
    path('konfirmasi_item', konfirmasi_item, name="konfirmasi_item")
]