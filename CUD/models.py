from django.db import models

class CreateItem(models.Model):
    nama = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=100)
    usia = models.CharField(max_length=100)
    bahan = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class UpdateItem(models.Model):
    nama = models.CharField(max_length=100)
    deskripsi = models.CharField(max_length=100)
    usia = models.CharField(max_length=100)
    bahan = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)

    def __str__(self):
        return self.title
# Create your models here.
