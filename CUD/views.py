from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ItemForm, UpdateItemForm
from .models import CreateItem, UpdateItem

response = {}

def item(request):
    html = 'item.html'
    response['fill_form'] = ItemForm
    return render(request, html, response)
	
def fill_form(request):
	if (request.method == 'POST'):
		response['nama'] = request.POST['nama'] 
		response['deskripsi'] = request.POST['deskripsi'] 
		response['usia'] = request.POST['usia']
		response['bahan'] = request.POST['bahan'] 
		pesan = CreateItem(nama=response['nama'], deskripsi=response['deskripsi'], 
							usia=response['usia'], bahan=response['bahan'])
		pesan.save()
		html = 'forms.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/item/')

def update(request):
    html = 'update_item.html'
    response['fill_form2'] = UpdateItemForm
    return render(request, html, response)
	
def fill_form2(request):
	if (request.method == 'POST'): 
		response['nama'] = request.POST['nama'] 
		response['deskripsi'] = request.POST['deskripsi'] 
		response['usia'] = request.POST['usia']
		response['bahan'] = request.POST['bahan']
		response['kategori'] = request.POST['kategori']
		update = UpdateItem(nama=response['nama'], 
							deskripsi=response['deskripsi'], usia=response['usia'], bahan = response['bahan'], kategori = response['kategori'])
		update.save()
		html = 'forms.html'
		return render(request, html, response)
	else:
		return HttpResponseRedirect('/item/')

def daftar_item(request):
	return render(request, 'daftaritem.html', response)

def delete_item(request):
	return render(request, 'Delete_item.html', response)

def konfirmasi_item(request):
	return render(request, 'confirm.html', response)