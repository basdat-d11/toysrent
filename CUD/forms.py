from django import forms

class ItemForm(forms.Form):
    nama_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi Nama Item'
    }
    deskripsi_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi nama item'
    }
    usia_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi rentang usia'
    }
    bahan_attrs = {
        'class' : 'form-control',
        'placeholder': 'Isi bahan'
    }
    KATEGORI_CHOICES = [
		('kategori a', 'KATEGORI A'),
		('kategori b', 'KATEGORI B'),
		('kategori c', 'KATEGORI C')
	]
	
    nama = forms.CharField(label="Nama", required=True, max_length=100, widget=forms.TextInput(attrs=nama_attrs))
    deskripsi = forms.EmailField(label="Deskripsi", required=True, max_length=100, widget=forms.TextInput(attrs=deskripsi_attrs))
    usia = forms.CharField(label="Usia", required=True, max_length=100, widget=forms.TextInput(attrs=usia_attrs))  
    bahan = forms.CharField(label="bahan", required=True, max_length=100, widget=forms.TextInput(attrs=bahan_attrs))
    kategori = forms.CharField(label='kategori', required=True, widget=forms.Select(choices=KATEGORI_CHOICES))


class UpdateItemForm(forms.Form):
    nama_attrs = {
        'class' : 'form-control',
        'placeholder': 'Nama Item'
    }
    deskripsi_attrs = {
        'class' : 'form-control',
        'placeholder': 'Deskripsi item'
    }
    usia_attrs = {
        'class' : 'form-control',
        'placeholder': 'Rentang usia'
    }
    bahan_attrs = {
		'class' : 'form-control',
		'placeholder': 'Bahan item'
    }
    KATEGORI_CHOICES = {
		('kategori a', 'KATEGORI A'),
		('kategori b', 'KATEGORI B'),
		('kategori c', 'KATEGORI C')
    }
	
    nama = forms.CharField(label="Nama", required=True, max_length=100, widget=forms.TextInput(attrs=nama_attrs))
    deskripsi = forms.EmailField(label="Deskripsi", required=True, max_length=100, widget=forms.TextInput(attrs=deskripsi_attrs))
    usia = forms.CharField(label="Usia", required=True, max_length=100, widget=forms.TextInput(attrs=usia_attrs))
    bahan = forms.CharField(label="bahan", required=True, max_length=100, widget=forms.TextInput(attrs=bahan_attrs))
    kategori = forms.CharField(label='kategori', required=True, widget=forms.Select(choices=KATEGORI_CHOICES))