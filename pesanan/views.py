from django.db import connection
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PesananForm
import random
import string
from datetime import datetime
from datetime import timedelta

response = {}

def pesanan(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    if request.method == 'POST':
        form = PesananForm(request.POST)
        if form.is_valid():
            anggota = form.cleaned_data['anggota']
            barang = form.cleaned_data['barang']
            sewa = form.cleaned_data['sewa']
            datetime_pemesanan = datetime.today().strftime('%Y') + "-" + datetime.today().strftime('%m') + "-" + datetime.today().strftime('%d')
            datetime_pengembalian = datetime.today() + timedelta(days=sewa)
            datetime_pengembalian = datetime_pengembalian.strftime('%Y') + "-" + datetime_pengembalian.strftime('%m') + "-" + datetime_pengembalian.strftime('%d')
            harga_sewa = 0
            tarif = 0
            status = "Available"
            kuantitas_barang = len(objects)

            lettersAndDigits = string.ascii_uppercase + string.digits
            while True:
                id_pemesanan = ''.join(random.choice(lettersAndDigits) for i in range(10))
                cursor.execute("select * from pemesanan where id_pemesanan = '" + id_pemesanan + "'")
                if len(cursor.fetchall())== 0:
                    break

            digits = string.digits


            for barang in barang:

                cursor.execute("select harga_sewa from info_barang_level ibl " +
                               "where ibl.nama_level in (" +
                               "select a.level from anggota a where no_ktp = '" + anggota + "')")
                harga_sewa += cursor.fetchone()[0]


            cursor.execute("insert into pemesanan values ('" + id_pemesanan + "','" + str(datetime_pemesanan) + "', " +
                           str(kuantitas_barang) + ", " + str(harga_sewa) + ", " + str(tarif) + ", '" +
                           anggota + "', '" + status + "')")

            for barang in barang:
                while True:
                    no_urut = ''.join(random.choice(digits) for i in range(10))
                    cursor.execute("select * from barang_pesanan where no_urut = '" + no_urut + "'")
                    if len(cursor.fetchall()) == 0:
                        break

                cursor.execute("insert into barang_pesanan values ('" + id_pemesanan + "', '" + no_urut + "', '" + \
                               barang + "', '" + datetime_pemesanan + "', " + str(sewa) + ", '" + \
                               datetime_pengembalian + "', '" + status + "')")

            return HttpResponseRedirect('/pesanan/pesananlist?status=create&id=' + id_pemesanan)
    else:
        form = PesananForm()
    return render(request,'pesanan.html', {'forms' : form})


def update(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    if request.method == 'POST':
        form = PesananForm(request.POST)
        if form.is_valid():
            return HttpResponseRedirect('')
    else:
        form = PesananForm()
    return render(request,'update_pesanan.html', {'forms' : form})


def daftar_pesanan(request):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')
    cursor.execute('select * from pemesanan')
    result = cursor.fetchall()
    response['requests'] = result
    for i in range(len(result)):
        cursor.execute("select nama_item from barang b where id_barang in " +
                       "(select id_barang from barang_pesanan bp where b.id_barang = bp.id_barang and bp.id_pemesanan = '" + 
					   result[i][0] + "')")
        nama_barang = cursor.fetchall()
        daftarPesanan = list(response['requests'][i])
        daftarPesanan.append(list(nama_barang))
        response['requests'][i] = daftarPesanan
    return render(request,'daftar_pesanan.html',response)


def delete_pesanan(request):
	return render(request, 'delete_pesanan.html', response)

def konfirmasi_pesanan(request):
	return render(request, 'konfirmasi.html', response)