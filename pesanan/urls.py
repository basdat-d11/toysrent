from django.urls import path
from .views import pesanan, update, daftar_pesanan, delete_pesanan, konfirmasi_pesanan

urlpatterns = [
    path('pesanan', pesanan, name="pesanan"),
    path('update_pesanan', update, name="update"),
    path('daftar_pesanan', daftar_pesanan, name="daftar_pesanan"),
    path('delete_pesanan', delete_pesanan, name="delete_pesanan"),
    path('konfirmasi', konfirmasi_pesanan, name="konfirmasi_pesanan")
]