from django.db import connection
from django import forms

class PesananForm(forms.Form):
    cursor = connection.cursor()
    cursor.execute('set search_path to toysrent_d11')

    cursor.execute('select * from anggota')
    all_anggota = cursor.fetchall()
    all_anggota = list(all_anggota)
    for i in range(len(all_anggota)):
        member = []
        member.append(all_anggota[i][0])
        member.append(all_anggota[i][0])
        all_anggota[i] = tuple(member)
    anggota_attrs = {
        'name': 'no_ktp_anggota',
        'id': 'form'
    }
    anggota = forms.CharField(label='Anggota', required=True, widget=forms.Select(choices=all_anggota, attrs=anggota_attrs))
    
    cursor.execute('select nama_item,id_barang from barang')
    all_barang = cursor.fetchall()
    all_barang = list(all_barang)
    for i in range(len(all_barang)):
        object = []
        object.append(all_barang[i][1])
        object.append(all_barang[i][0] + " - " + all_barang[i][1])
        all_barang[i] = tuple(object)
    barang_attrs = {
        'name': 'barang',
        'id' : 'form'
    }
    barang = forms.CharField(label='Barang', required=True, widget=forms.Select(choices=all_barang, attrs=barang_attrs))
    
    sewa_attrs = {
        'name': 'lama_sewa',
        'id': 'form',
        'placeholder': '5'
    }
    sewa = forms.IntegerField(label="Lama Sewa", required=True, widget=forms.TextInput(attrs=sewa_attrs))