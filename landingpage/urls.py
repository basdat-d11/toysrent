from django.urls import path
from .views import landingpage

urlpatterns = [
	path(r'landingpage', landingpage),
	path(r'', landingpage)
]