from django.db import models

# Create your models here.
class PengirimanModel(models.Model):
    barang = models.CharField(max_length=100)
    alamat = models.CharField(max_length=100)
    tanggal = models.DateField()
    metode = models.CharField(max_length=100)

    def __str__(self):
        return self.title

class UpdatePengirimanModel(models.Model):
    barang = models.CharField(max_length=100)
    alamat = models.CharField(max_length=100)
    tanggal = models.DateField()
    metode = models.CharField(max_length=100)

    def __str__(self):
        return self.title