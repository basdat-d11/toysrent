from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import PengirimanForm, UpdatePengirimanForm
from .models import PengirimanModel, UpdatePengirimanModel

response = {}

def pengiriman(request):
    html = 'pengiriman.html'
    response['fill_form'] = PengirimanForm
    return render(request, html, response)
	
def fill_form(request):
    if (request.method == 'POST'):
        response['alamat'] = request.POST['alamat'] 
        response['barang'] = request.POST['barang'] 
        response['tanggal'] = request.POST['tanggal'] 
        response['metode'] = request.POST['metode']
        kirim = PengirimanModel(alamat=response['alamat'], barang=response['barang'],tanggal=response['tanggal'], metode=response['metode'])
        kirim.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/pengiriman/')

def update(request):
    html = 'update_pengiriman.html'
    response['fill_form2'] = UpdatePengirimanForm
    return render(request, html, response)
	
def fill_form2(request):
    if (request.method == 'POST'): 
        response['barang'] = request.POST['barang']
        response['alamat'] = request.POST['alamat']  
        response['tanggal'] = request.POST['tanggal'] 
        response['metode'] = request.POST['metode']
        update = UpdatePengirimanModel(alamat=response['alamat'], barang=response['barang'], 
							tanggal=response['tanggal'], metode=response['metode'])
        update.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/pengiriman/')

def daftar_pengiriman(request):
	return render(request, 'daftar_pengiriman.html', response)

def delete_pengiriman(request):
	return render(request, 'delete_pengiriman.html', response)

def konfirmasi_pengiriman(request):
	return render(request, 'konfirmasi.html', response)
