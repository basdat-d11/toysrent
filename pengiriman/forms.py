from django import forms

pilihan_barang= [
    ('...', '...')
    ]
pilihan_alamat= [
    ('...', '...')
    ]

class PengirimanForm(forms.Form):
    barang_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi barang yang ingin dikirim'
    }
    alamat_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi alamat'
    }
    tanggal_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi tanggal pengiriman'
    }
    metode_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi metode pengiriman'
    }
    barang = forms.CharField(label="Barang pesanan", required=True, max_length=100, widget=forms.Select(choices=pilihan_barang))
    alamat = forms.CharField(label="Alamat tujuan", required=True, max_length=100, widget=forms.Select(choices=pilihan_alamat))
    tanggal = forms.DateField(label="Tanggal pengiriman", required=True, widget=forms.DateInput(attrs=tanggal_attrs))
    metode = forms.CharField(label="Metode", required=True, max_length=100, widget=forms.TextInput(attrs=tanggal_attrs))


class UpdatePengirimanForm(forms.Form):
    barang_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi barang yang ingin dikirim'
    }
    alamat_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi alamat'
    }
    tanggal_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi tanggal pengiriman'
    }
    metode_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi metode pengiriman'
    }
    barang = forms.CharField(label="Barang pesanan", required=True, max_length=100, widget=forms.Select(choices=pilihan_barang))
    alamat = forms.CharField(label="Alamat tujuan", required=True,max_length=100, widget=forms.Select(choices=pilihan_alamat))
    tanggal = forms.DateField(label="Tanggal pengiriman", required=True, widget=forms.DateInput(attrs=tanggal_attrs))
    metode = forms.CharField(label="Metode", required=True, max_length=100, widget=forms.TextInput(attrs=tanggal_attrs))
