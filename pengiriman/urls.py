from django.urls import path
from .views import pengiriman, update, delete_pengiriman, daftar_pengiriman, konfirmasi_pengiriman

# app_name = 'pengiriman'
urlpatterns = [
    path('buat_pengiriman', pengiriman, name='pengiriman'),
    path('update_pengiriman', update, name="update"),
    path('daftar_pengiriman', daftar_pengiriman, name="daftar_pengiriman"),
    path('delete_pengiriman', delete_pengiriman, name="delete_pengiriman"),
    path('konfirmasi_pengiriman', konfirmasi_pengiriman, name="konfirmasi_pengiriman")
]