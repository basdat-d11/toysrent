from django import forms

pilihan_barang= [
    ('...', '...')
    ]

class ReviewForm(forms.Form):
    barang_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi barang yang ingin direview'
    }
    review_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi review'
    }
    barang = forms.CharField(label="Barang pesanan", required=True, max_length=100, widget=forms.Select(choices=pilihan_barang))
    review = forms.CharField(label="Review", required=True, widget=forms.TextInput(attrs=review_attrs))


class UpdateReviewForm(forms.Form):
    barang_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi barang yang ingin direview'
    }
    review_attrs = {
        'id': 'text',
        'class' : 'form-control',
        'placeholder': 'Isi review'
    }
    barang = forms.CharField(label="Barang pesanan", required=True, max_length=100, widget=forms.Select(choices=pilihan_barang))
    review = forms.CharField(label="Review", required=True, widget=forms.TextInput(attrs=review_attrs))