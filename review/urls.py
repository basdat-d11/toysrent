from django.urls import path
from .views import review, update, delete_review, daftar_review, konfirmasi_review

# app_name = 'review'
urlpatterns = [
    path('buat_review', review, name='review'),
    path('update_review', update, name="update"),
    path('daftar_review', daftar_review, name="daftar_review"),
    path('delete_review', delete_review, name="delete_review"),
    path('konfirmasi_review', konfirmasi_review, name="konfirmasi_review")
]