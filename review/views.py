from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import ReviewForm, UpdateReviewForm
from .models import ReviewModel, UpdateReviewModel

response = {}

def review(request):
    html = 'review.html'
    response['fill_form'] = ReviewForm
    return render(request, html, response)
	
def fill_form(request):
    if (request.method == 'POST'):
        response['barang'] = request.POST['barang'] 
        response['review'] = request.POST['review']
        pesan = ReviewModel(barang=response['barang'],review=response['review'])
        pesan.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/review/')

def update(request):
    html = 'update_review.html'
    response['fill_form2'] = UpdateReviewForm
    return render(request, html, response)
	
def fill_form2(request):
    if (request.method == 'POST'): 
        response['barang'] = request.POST['barang']
        response['alamat'] = request.POST['alamat']  
        response['review'] = request.POST['review'] 
        response['metode'] = request.POST['metode']
        update = UpdateReviewModel(alamat=response['alamat'], barang=response['barang'], 
							review=response['review'], metode=response['metode'])
        update.save()
        html = 'forms.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/review/')

def daftar_review(request):
	return render(request, 'daftar_review.html', response)

def delete_review(request):
	return render(request, 'delete_review.html', response)

def konfirmasi_review(request):
	return render(request, 'konfirmasi.html', response)
